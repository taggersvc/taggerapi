## TaggerAPI documentations

## Requirements:
~~~
- install postgresql database
$ pip install -r requirements.txt
~~~

## Operate:
~~~
$ python tagger/manage.py migrate
$ python tagger/manage.py createsuperuser (if u want a admin site..)
$ python tagger/manage.py runserver
~~~

## Preparing main tags csv
```
$ python tagger/prepare_main_tags.py

This will create a file main-tags.csv in datas/tags directory.
```

## Import data
~~~
$ python tagger/manage.py import_tags datas/tags/main-tags.csv
$ python tagger/manage.py import_tag_details datas/details/
~~~

## Store docker image in docker hub (Private repo) (Development Use)
~~~
$ sudo docker login --username <user> --password <pass>
$ sudo docker build -t web .
$ sudo docker tag web:latest smanjil/taggerapi
$ sudo docker push smanjil/taggerapi
~~~

## Docker Compose
```
$ docker-compose up -d --build
$ docker-compose run api python /code/tagger/manage.py makemigrations
$ docker-compose run api python /code/tagger/manage.py migrate
$ docker-compose run api python /code/tagger/prepare_main_tags.py
$ docker-compose run api python /code/tagger/manage.py import_tags /code/datas/tags/main-tags.csv
$ docker-compose run api python /code/tagger/manage.py import_tag_details /code/datas/details
```

## DRF Token Authenications
```
- Getting token authentication for a particular user
$ http post http://127.0.0.1:8000/api-token-auth/ username=ano password=12345
    
- Use that token to access api
$ http http://127.0.0.1:8000/api/questions/ 'Authorization: Token 4011459179642b9f4b85f95674c436c935279e84'
$ http http://127.0.0.1:8000/api/tags/ 'Authorization: Token 4011459179642b9f4b85f95674c436c935279e84'
```

## Things to keep in mind before working on project:
- Maintain proper README.
- Prepare .gitignore file.
- Create requirements.txt file to hold project dependencies with version.
- Maintain .gitlab-ci.yml file to cover stages for the buildchain for project.
- Logging

## Todos:
- Check for duplicates for inserting main tags & detail.

