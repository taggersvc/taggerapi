"""Import main tags management script."""
import logging

from django.core.management.base import BaseCommand, CommandError

from tags.management.utils.read_file import read_file
from tags.management.utils.save_data import import_main_tags


# logger instance
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Class to handle import tags command."""

    help = 'Import main tags to database.'

    def add_arguments(self, parser):
        """Cli argument handler.

        Args:
            parser: Argument parser.
        """
        parser.add_argument('fpath', nargs='+', type=str)

    def handle(self, *args, **options):
        """Command handler.

        Args:
            **args: CLI argument.
            **options: Option handlers.
        """
        fpath = options['fpath'][0]

        # read file
        try:
            file_object = read_file(fpath)
            logger.info(f"Received {file_object}.")
        except Exception:
            logger.error(f"{fpath} does not exist.")
            raise CommandError(f'File {fpath} does not exist.')

        # pass file object to import tags script
        import_main_tags(file_object)
