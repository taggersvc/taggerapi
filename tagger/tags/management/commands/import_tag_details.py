"""Import tag details script."""
import logging
import os

from django.core.management.base import BaseCommand, CommandError

from tags.management.utils.read_file import read_file
from tags.management.utils.save_data import import_details


# logger instance
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Class to handle import tags command."""

    help = 'Import tag details to database.'

    def add_arguments(self, parser):
        """Cli argument handler.

        Args:
            parser: Argument parser.
        """
        parser.add_argument('fpath', nargs='+', type=str)

    def handle(self, *args, **options):
        """Command handler.

        Args:
            **args: CLI argument.
            **options: Option handlers.
        """
        fpath = options['fpath'][0]

        for files in os.listdir(fpath):
            path = os.path.join(fpath, files)
            # read file
            try:
                file_object = read_file(path)
                logger.info(f"Received {file_object}.")
            except Exception:
                logger.error(f"{fpath} does not exist.")
                raise CommandError(f'File {path} does not exist.')

            # pass file object to import tags script
            import_details(file_object, files.split('stack-')[1].split('.')[0])
