"""Read file script."""
import csv
import logging


# logger instance
logger = logging.getLogger(__name__)


def read_file(fpath):
    """Read file.

    Args:
        fpath: file path to read

    Returns: csv file reader object.
    """
    logger.info(f"Reading file {fpath}.")
    fi = open(fpath)
    csv_reader = csv.reader(fi, delimiter=',')

    # exclude header
    next(csv_reader, None)

    return csv_reader
