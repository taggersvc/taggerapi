"""Main import script."""
import logging
from tags.models import Question, Tag


# logger instance
logger = logging.getLogger(__name__)


TAG_OBJECT_SET = False


def import_main_tags(file_data):
    """Import tags to database.

    Args:
        file_data: file object.
    """
    main_tags_count = 0
    logger.info("Start import of main tags.")
    for row in file_data:
        Tag.objects.create(tag_name=row[0])
        main_tags_count += 1
    logger.info(f"Complete import of {main_tags_count} main tags.\n")


def get_tag_object(tag_name):
    """Get tag object.

    Args:
        tag_name: tag name to get foregin object

    Returns:tag object

    """
    global TAG_OBJECT_SET

    logger.info(f"Getting tag object for {tag_name}")
    tag = Tag.objects.get(tag_name=tag_name)

    TAG_OBJECT_SET = True
    logger.info(f"TAG_OBJECT set to {TAG_OBJECT_SET}")

    return tag


def import_details(file_data, tag_name):
    """Import tag details to database.

    Args:
        file_data: file objecz.
    """
    global TAG_OBJECT_SET

    bulk_detail_objects = list()

    logger.info("Start import of detail tags.")
    logger.info("Preparing Question model object.")
    # insert in details table
    for row in file_data:
        if not TAG_OBJECT_SET:
            tag = get_tag_object(tag_name)

        q_obj = Question(
            question=row[1],
            url=row[2],
            tags=row[0],
            tag_id=tag
        )
        tag_name = tag_name

        bulk_detail_objects.append(q_obj)

    TAG_OBJECT_SET = False
    logger.info(f"TAG_OBJECT set to {TAG_OBJECT_SET}")

    # bulk save
    Question.objects.bulk_create(bulk_detail_objects)
    logger.info(f"Imported {len(bulk_detail_objects)} {tag_name}.\n")
