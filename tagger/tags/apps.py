"""Tags app file."""

from django.apps import AppConfig


class TagsConfig(AppConfig):
    """Tags config class."""

    name = 'tags'
