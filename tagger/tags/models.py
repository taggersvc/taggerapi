"""Tags app model."""

from django.db import models


class Tag(models.Model):
    """Parent tag model."""

    tag_name = models.CharField(max_length=50)

    def __str__(self):
        """Return tag name to admin."""
        return self.tag_name


class Question(models.Model):
    """Question model."""

    question = models.CharField(max_length=500)
    url = models.CharField(max_length=500)
    tags = models.CharField(max_length=255)
    tag_id = models.ForeignKey(Tag, on_delete='cascade')

    def __str__(self):
        """Return question to admin."""
        return self.question
