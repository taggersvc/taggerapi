
import os


DATA_DIR = os.path.join(os.getcwd(), 'datas/details')
TAG_FILE = os.path.join(os.getcwd(), 'datas/tags/main-tags.csv')


def get_main_tags():
    with open(TAG_FILE, 'w') as fo:
        fo.write('tags' + '\n')
        for file_name in os.listdir(DATA_DIR):
            tag = file_name.split('stack-')[1].split('.')[0]
            fo.write(tag + '\n')


if __name__ == '__main__':
    get_main_tags()
