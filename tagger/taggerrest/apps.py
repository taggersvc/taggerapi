"""DRF app."""
from django.apps import AppConfig


class TaggerrestConfig(AppConfig):
    """Taggerrest app config."""

    name = 'taggerrest'
