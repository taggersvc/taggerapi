"""Taggerrest api serializer."""
from rest_framework import serializers

from tags.models import Tag, Question


class TagSerializer(serializers.HyperlinkedModelSerializer):
    """Tag serializer class."""

    class Meta:
        """Model instantiate for Tag and field association."""

        model = Tag
        fields = ('tag_name', )


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    """Question serializer class."""

    class Meta:
        """Model instantiate for Question and field association."""

        model = Question
        fields = ('question', 'url', 'tags', 'tag_id', )
