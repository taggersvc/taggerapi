"""Taggerrest viewsets."""
from tags.models import Tag, Question
from rest_framework import viewsets
from taggerrest.serializers import TagSerializer, QuestionSerializer
from rest_framework.permissions import IsAuthenticated


class TagViewSet(viewsets.ModelViewSet):
    """API endpoint for Tag."""

    permission_classes = (IsAuthenticated,)

    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class QuestionViewSet(viewsets.ModelViewSet):
    """API endpoint for Question."""

    permission_classes = (IsAuthenticated,)

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
